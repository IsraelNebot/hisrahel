# -*- coding: utf-8 -*-

# Importo las funciones de turtle

from turtle import *

tortuga = Turtle()
pantalla = Screen()

#pido los datos por pantalla

peldaños = int(input('Cuantos peldaños tiene la escalera? '))
longitud = int(input('Que longitud tienen los peldaños? '))

# Hago el dibujo

tortuga.forward(peldaños*longitud)
tortuga.left(90)
tortuga.forward(longitud)
for vertical in range(1,peldaños):
    tortuga.left(90)
    tortuga.forward(longitud)
    for horizontal in range(1):
        tortuga.right(90)
        tortuga.forward(longitud)

tortuga.left(90)
tortuga.forward(longitud)
tortuga.left(90)
tortuga.forward(longitud*peldaños)
