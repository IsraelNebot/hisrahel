
T = float(input('Introduce la temperatura del aire: '))
V = float(input('Introduce la velocidad del viento: '))

Ts = 13.12+(0.6215*T) - (11.37*(V**0.16))+(0.3965*T)*(V**0.16)

print('La temperatura de sensación es de {0:.2f} grados Celsius'.format(Ts))
